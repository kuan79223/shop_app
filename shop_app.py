from flask import Flask, request, abort
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///consumer.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# 客戶數據庫
class User(db.Model):
    __tablename__ = 'user'

    id = db.Column('id', db.Integer, primary_key=True)
    email = db.Column('email', db.String(50), nullable=False)
    phone = db.Column('phone', db.String(12), nullable=False)
    timestamp = db.Column('timestamp', db.DateTime, default=datetime.utcnow)

    order_user = db.relationship('Orders', backref='user', uselist=False)

    def __init__(self, id, email, phone, timestamp):
        self.id = id
        self.email = email
        self.phone = phone
        self.timestamp = timestamp
        

# 訂單數據庫
class Orders(db.Model):
    __tablename__ = 'order'

    id = db.Column('id', db.Integer, primary_key=True)
    amount = db.Column('amount', db.Integer, nullable=False)
    timestamp = db.Column('timestamp', db.DateTime, default=datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))

    def __init__(self, amount, timestamp, user_id, item_id):

        self.amount = amount
        self.timestamp = timestamp
        self.user_id = user_id
        self.item_id = item_id


# 商品項目數據庫
class Item(db.Model):
    __tablename__ = 'item'

    id = db.Column('id', db.Integer, primary_key=True) # autoincrement=True) 自動生成id
    item = db.Column('item', db.String(100), nullable=False)
    price = db.Column('price', db.Integer, nullable=False)
    timestamp = db.Column('timestamp', db.DateTime, default=datetime.utcnow)

    order_item = db.relationship('Orders', backref='item', uselist=False)

    def __init__(self, id, item, price, timestamp):
        self.id = id
        self.item = item
        self.price = price
        self.timestamp = timestamp


with app.app_context():
    db.create_all()
    # 新增販售商品
    add_items = [
        Item(id=1, item='商品A', price=100, timestamp=datetime.utcnow()),
        Item(id=2, item='商品B', price=200, timestamp=datetime.utcnow()),
        Item(id=3, item='商品C', price=300, timestamp=datetime.utcnow())
    ]

    for item in add_items:
        existing_item = Item.query.get(item.id)
        if existing_item is None:
            db.session.add(item)
            db.session.commit()  # 提交更改，將資料寫入數據庫


@app.route('/clients')
def clients():
    user_data = db.session.query(User).all()
    users = {}
    for us in user_data:
        users[us.id] = {'email': us.email, 'phone': us.phone}

    return users

@app.route('/clients/<int:us_id>', methods=['GET', 'POST'])
def client(us_id):
    user_data = {}

    if request.method == 'GET':

        # 判斷是否在資料庫中
        user = User.query.filter_by(id=us_id).first()

        if user is None:
            abort(404, 'Could not find client with that id')
            

        user_data[us_id] = {'email': user.email, 'phone': user.phone}

        return user_data

    if request.method == 'POST':

        # 判斷是否在資料庫中
        user = User.query.filter_by(id=us_id).first()

        if user:
            abort(401, 'client is exist')
            
        email = request.json.get('email')
        phone = request.json.get('phone')
        # print(email, phone)
        # 沒有則加入資料庫
        user_data = User(id=us_id, email=email, phone=phone, timestamp=datetime.utcnow())

        db.session.add(user_data)
        db.session.commit()

        return 'success add client data'

@app.route('/items')
@app.route('/items/<int:us_id>', methods=['GET', 'POST'])
def items(us_id=None):
    item_dict = {}

    if us_id is None:
        sale_items = Item.query.all()

        if sale_items:
            for item in sale_items:
                item_id = item.id
                item_name = item.item
                item_price = item.price
                item_dict[item_id] = {'item': item_name, 'price': item_price}

        return item_dict

    else:
        item_dict = items()
        show_item = {}
        if request.method == 'POST':

            item_id = request.json.get('item_id')
            amount = request.json.get('amount')

            if item_id in item_dict.keys():
                print(item_dict.keys())

                purchasing = Orders(user_id=us_id, item_id=item_id, amount=amount, timestamp=datetime.utcnow())

                db.session.add(purchasing)
                db.session.commit()

                show_item[us_id] = {'item_id': purchasing.item_id, 'amount': purchasing.amount, 'timestamp': purchasing.timestamp}
                
                return show_item

        if request.method == 'GET':
            
            order_data = {}
            item_list = Item.query.all()
            orders = Orders.query.all()
            total_cost = 0  
        
            for ord in orders:
                item_id = ord.item_id
                amount = ord.amount
                for item in item_list:
                    price = item.price
                # 比對 url與 表單內的user_id 是否相同
                    if ord.user_id == us_id and ord.item.id == item.id:
                        if item_id in order_data:
                            
                    # 計算幾筆
                            order_data[item_id]['num'] += 1
                            order_data[item_id]['amount'] += amount
                            order_data[item_id]['price'] += price
                            
                        else:
                            order_data[item_id] = {'num': 1, 'amount': amount, 'price': price}


                        total_cost = order_data[item_id]['amount'] * order_data[item_id]['price']
                        order_data[item_id]['total'] = total_cost 
                                                  
            print(order_data)

            return order_data


if __name__ == '__main__':
    app.run(debug=True, port=5001)
