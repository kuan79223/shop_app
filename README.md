# Flask 購物訂單專案

這是一個使用 Python Flask 框架的購物訂單專案，使用RESTful的GET和POST與使用者互動。

## 使用虛擬環境(venv)

在專案中使用虛擬環境來隔離專案所需的 Python 套件。

## 依賴套件

專案所需的python 套件列於 "requirements.txt" 檔案中。

## 建立虛擬環境與啟動

"shell"
$ python -m venv venv


在 Windows 環境中，啟動虛擬環境：

"shell"
$ venv\Scripts\activate

在 macOS/Linux 環境中，啟動虛擬環境：

"shell"
$ source venv/bin/activate


## 安裝所需套件與啟動應用程式

"shell"  
$ python -m venv venv  
$ pip install -r requirements.txt  
$ python shop_app.py  


在 postman 模擬 
使用 GET 訪問 http://localhost:5001/clients 可以看到客戶資料總表  

使用 GET 請求 http://127.0.0.1:5001/clients/(客戶的id代碼) 可以看到單一客戶資料 

使用 POST 請求 http://127.0.0.1:5001/clients/(客戶的id代碼) 使用JSON的型態新增新的客戶資料

{
    "email": "client2@example.com",
    "phone": "0976418888"
}

使用 GET 訪問 http://127.0.0.1:5001/items 可以看到預設的販售表單  

使用 POST 請求 http://127.0.0.1:5001/items/(客戶的id代碼) 使用JSON的型態來增加訂單

{
    "item_id": 3,
    "amount": 30
}
 
使用 GET 請求 http://127.0.0.1:5001/items/(客戶的id代碼) 可以看到以JSON的形式展示單一客戶訂購的資料數據  
